import React, { useContext } from 'react'
import { Form, Input, Button, Checkbox } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { Redirect } from 'react-router-dom'
import styles from './LoginForm.scss'
import { BACKEND_URL, UserContext } from '../../App'
import {
  userLoginFailed,
  userLoginRequested,
  userLoginSuccess,
} from '../HookStateManagement/userAction'

import axios from 'axios'

const LoginForm = () => {
  const { user, userDispatch } = useContext(UserContext)
  // console.log(user)

  const onFinish = (values: any) => {
    const username = values.username
    const password = values.password
    userDispatch(userLoginRequested())
    axios({
      url: `http://${BACKEND_URL}/users/login`,
      method: 'post',
      data: {
        username: username,
        password: password,
      },
    })
      .then((response: { data: any }) => {
        // console.log(response);
        const data = response.data
        if (!data.error) {
          userDispatch(userLoginSuccess(response.data.userInfo))
        } else {
          userDispatch(userLoginFailed(response.data.error))
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return user.isAuthenticated ? (
    <Redirect to="/management" />
  ) : (
    <Form
      name="normal_login"
      className={styles.login_form}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="username"
        rules={[
          {
            required: true,
            message: 'Please input your Username!',
          },
        ]}
      >
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="Username"
        />
      </Form.Item>
      {user.error && <p style={{ color: 'red' }}>* {user.error}</p>}
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your Password!',
          },
        ]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>

      <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>Remember me</Checkbox>
        </Form.Item>
      </Form.Item>

      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          className={styles.login_form_button}
        >
          Log in
        </Button>
      </Form.Item>
    </Form>
  )
}

export default LoginForm
