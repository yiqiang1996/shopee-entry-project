import {
  USER_LOGIN_REQUESTED,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  USER_LOGOUT,
} from './userAction'

export const userReducer = (
  state: {
    loading: boolean
    isAuthenticated: boolean
    error: string
    userInfo: any
  },
  action: { type: any; payload?: any }
) => {
  switch (action.type) {
    case USER_LOGIN_REQUESTED:
      return {
        ...state,
        loading: true,
        error: '',
      }
    case USER_LOGIN_SUCCESS:
      return {
        loading: false,
        userInfo: action.payload,
        isAuthenticated: true,
        error: '',
      }
    case USER_LOGIN_FAILED:
      return {
        loading: false,
        userInfo: {},
        isAuthenticated: false,
        error: action.payload,
      }
    case USER_LOGOUT:
      return {
        loading: false,
        userInfo: {},
        isAuthenticated: false,
        error: '',
      }
    default:
      return state
  }
}
