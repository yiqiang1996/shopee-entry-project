const USER_LOGIN_REQUESTED = 'USER_LOGIN_REQUESTED'
const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS'
const USER_LOGIN_FAILED = 'USER_LOGIN_FAILED'
const USER_LOGOUT = 'USER_LOGOUT'

const userLoginRequested = () => {
  return {
    type: USER_LOGIN_REQUESTED,
  }
}

const userLoginSuccess = (userInfo: {
  name: string
  email: string
  role: string
  image: string
}) => {
  return {
    type: USER_LOGIN_SUCCESS,
    payload: userInfo,
  }
}

const userLoginFailed = (errorMessage: string) => {
  return {
    type: USER_LOGIN_FAILED,
    payload: errorMessage,
  }
}

const userLogOut = () => {
  return {
    type: USER_LOGOUT,
  }
}

export {
  USER_LOGIN_REQUESTED,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  USER_LOGOUT,
  userLoginRequested,
  userLoginSuccess,
  userLoginFailed,
  userLogOut,
}
