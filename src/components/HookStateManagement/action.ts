const GET_EMPLOYEE = 'GET_EMPLOYEE'
const UPDATE_EMPLOYEE_SUCCESS = 'UPDATE_EMPLOYEE_SUCCESS'
const UPDATE_EMPLOYEE_FAILED = 'UPDATE_EMPLOYEE_FAILED'
const APPLY_FILTERS = 'APPLY_FILTERS'
const REMOVE_FILTERS = 'REMOVE_FILTERS'

interface Employee {
  name: string
  job: string
  entry_date: string
}

interface Response {
  total: number
  data: Employee[]
  current: number
  pageSize: number
}

const getEmployee = (payload: Response) => {
  return {
    type: GET_EMPLOYEE,
    payload: payload,
  }
}

const applyFitlers = (filters: any) => {
  return {
    type: APPLY_FILTERS,
    payload: filters,
  }
}

const removeFilters = () => {
  return {
    type: REMOVE_FILTERS,
  }
}

const updateEmployeeSuccess = (employee: Employee) => {
  return {
    type: UPDATE_EMPLOYEE_SUCCESS,
    payload: employee,
  }
}

const updateEmployeeFailed = () => {
  return {
    type: UPDATE_EMPLOYEE_FAILED,
  }
}

export {
  getEmployee,
  updateEmployeeSuccess,
  updateEmployeeFailed,
  applyFitlers,
  removeFilters,
  GET_EMPLOYEE,
  UPDATE_EMPLOYEE_SUCCESS,
  UPDATE_EMPLOYEE_FAILED,
  APPLY_FILTERS,
  REMOVE_FILTERS,
}
