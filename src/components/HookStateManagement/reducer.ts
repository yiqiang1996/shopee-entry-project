import {
  GET_EMPLOYEE,
  UPDATE_EMPLOYEE_SUCCESS,
  UPDATE_EMPLOYEE_FAILED,
  REMOVE_FILTERS,
  APPLY_FILTERS,
} from './action'

interface Filter {
  name?: string
  job?: string
  start_date?: string
  end_date?: string
}

export const reducer = (
  state: {
    loading: any
    data: any
    error: any
    pagination: any
    filters?: Filter
  },
  action: { type: any; payload?: any }
) => {
  switch (action.type) {
    case GET_EMPLOYEE:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        pagination: {
          ...state.pagination,
          current: action.payload.current,
          pageSize: action.payload.pageSize,
          total: action.payload.total,
        },
      }

    case UPDATE_EMPLOYEE_SUCCESS:
      const tempData = [...state.data]
      const tempIndex = tempData.findIndex(
        (item) => action.payload._id === item._id
      )
      const tempItem = tempData[tempIndex]
      tempData.splice(tempIndex, 1, { ...tempItem, ...action.payload })
      return {
        ...state,
        data: tempData,
        loading: false,
        error: '',
      }
    case UPDATE_EMPLOYEE_FAILED:
      return {
        ...state,
      }

    case REMOVE_FILTERS:
      return {
        ...state,
        filters: {},
        loading: true,
        data: [],
      }

    case APPLY_FILTERS:
      const filters = action.payload
      return {
        ...state,
        filters: { ...filters },
        loading: true,
        data: [],
      }
    default:
      return state
  }
}
