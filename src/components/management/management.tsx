import React, { useContext, lazy, Suspense } from 'react'
import { Layout } from 'antd'
import Nav from './Nav/Nav'
import { Switch, Route, useRouteMatch, Redirect } from 'react-router-dom'
import { UserContext } from '../../App'

const Sidebar = lazy(() => import('./Sidebar/Sidebar'))
const Table = lazy(() => import('./Table/Table'))
const Chart = lazy(() => import('./Chart/Chart'))

function Management() {
  const match = useRouteMatch()
  const userContext = useContext(UserContext)
  const user = userContext.user

  return (
    <Layout className="management">
      <Nav />
      <Layout className="content">
        <Suspense fallback={<div>Loading ...</div>}>
          <Switch>
            <Route exact path={match.path}>
              {user.isAuthenticated ? (
                <Redirect to="management/table" />
              ) : (
                <Redirect to="/login" />
              )}
            </Route>
            <Route path={`${match.path}/table`}>
              {user.isAuthenticated ? (
                <>
                  <Sidebar />
                  <Table />
                </>
              ) : (
                <Redirect to="/login" />
              )}
            </Route>
            <Route path={`${match.path}/chart`}>
              {user.isAuthenticated ? (
                <>
                  <Sidebar />
                  <Chart />
                </>
              ) : (
                <Redirect to="/login" />
              )}
            </Route>
            <Route path={`*`}>
              <Redirect to="/login" />
            </Route>
          </Switch>
        </Suspense>
      </Layout>
    </Layout>
  )
}

export default Management
