import { Layout, Breadcrumb, Select, Button, Space } from 'antd'
import { Column } from '@ant-design/charts'
import React, { useState, useEffect, useReducer } from 'react'

import styles from './chart.scss'
import moment from 'moment'
import axios from 'axios'
import { BACKEND_URL } from '../../../App'

const { Content } = Layout
const { Option } = Select

interface Count {
  _id: string
  count: number
}

interface State {
  minYear: number
  maxYear: number
  data: Count[]
  years: number[]
}

const initialState = {
  minYear: 2021,
  maxYear: 2021,
  data: [],
  years: [],
}

const reducer = (state: State, action: { type: string; payload?: any }) => {
  switch (action.type) {
    case 'UPDATE_DATA':
      return {
        ...state,
        minYear: action.payload.years[0],
        maxYear: action.payload.years[action.payload.years.length - 1],
        years: action.payload.years,
        data: action.payload.data,
      }
    default:
      return {
        ...state,
      }
  }
}

function Chart() {
  const [year, setYear] = useState<number>(2021)
  const [groupBy, setGroupBy] = useState<string>('month')

  const [state, dispatch] = useReducer(reducer, initialState)

  const minYear: number = state.minYear
  const maxYear: number = state.maxYear
  const data: Count[] = state.data
  const years: number[] = state.years

  useEffect(() => {
    axios({
      method: 'get',
      url: `http://${BACKEND_URL}/employees/group`,
      params: { year, groupBy },
    }).then((response) => {
      console.log(response.data)
      dispatch({ type: 'UPDATE_DATA', payload: response.data })
    })
  }, [year, groupBy])

  const onGroupbyChange = (groupBy: string) => {
    setGroupBy(groupBy)
  }

  const onYearChange = (year: number) => {
    if (year) {
      setYear(year)
    } else {
      setYear(moment().year())
    }
  }

  const increaseYear = () => {
    setYear((current) => {
      const index = years.findIndex((year) => year > current)
      return years[index]
    })
  }

  const decreaseYear = () => {
    setYear((current) => {
      const index = years.findIndex((year) => year === current)
      return years[index - 1]
    })
  }

  const disablePrev = year === minYear ? true : false
  const disableNext = year === maxYear ? true : false

  const options: any[] = []
  for (const year of years) {
    options.push(
      <Option value={year} key={year}>
        {year}
      </Option>
    )
  }

  const config = {
    data: data,
    xField: '_id',
    yField: 'count',
    label: {
      position: 'middle',
      style: {
        fill: '#FFFFFF',
        opacity: 1,
      },
    },
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: false,
      },
    },

    tooltip: {
      title: (data: any) => {
        return year + ' ' + data
      },
    },

    meta: {
      count: { alias: 'New Hires' },
    },
  }

  return (
    <Layout className={styles.chart}>
      <Content>
        <Breadcrumb className={styles.title}>
          <Breadcrumb.Item>System Management</Breadcrumb.Item>
          <Breadcrumb.Item>Chart Page</Breadcrumb.Item>
        </Breadcrumb>

        <div className={styles.column}>
          <div className={styles.chartTitle}>Chart Page</div>
          <div className={styles.columnTitle}>{year} New Hire Counts</div>
          <Column {...config} />
          <div className={styles.yearPicker}>
            <Space>
              <Button onClick={decreaseYear} disabled={disablePrev}>
                Prev
              </Button>
              <Button onClick={increaseYear} disabled={disableNext}>
                Next
              </Button>
            </Space>

            <div className={styles.normalFont}>Group By</div>
            <Select
              allowClear
              style={{ width: 150, marginRight: 20 }}
              defaultValue="month"
              onChange={onGroupbyChange}
            >
              <Option value="month">Month</Option>
              <Option value="job">Job</Option>
            </Select>

            <div className={styles.normalFont}>Go to</div>
            <Select
              allowClear
              style={{ width: 150 }}
              placeholder="Select a Year"
              optionFilterProp="children"
              onChange={onYearChange}
            >
              {options}
            </Select>
          </div>
        </div>
      </Content>
    </Layout>
  )
}

export default Chart
