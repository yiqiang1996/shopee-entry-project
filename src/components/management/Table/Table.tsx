import React from 'react'
import Filter from './Filter/Filter'
import styles from './Table.scss'
import Display from './Display/Display'
import { Layout, Breadcrumb } from 'antd'

const { Content } = Layout

function Table() {
  return (
    <Layout className={styles.table}>
      <Content>
        <Breadcrumb className={styles.title}>
          <Breadcrumb.Item>System Management</Breadcrumb.Item>
          <Breadcrumb.Item>Table Page</Breadcrumb.Item>
        </Breadcrumb>
        <Filter />
        <Display />
      </Content>
    </Layout>
  )
}

export default Table
