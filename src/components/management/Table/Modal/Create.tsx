import React, { useState, useContext } from 'react'
import {
  Modal,
  Tabs,
  Form,
  Input,
  Select,
  DatePicker,
  message,
  Upload,
} from 'antd'

import { getEmployee } from '../../../HookStateManagement/action'
import { InboxOutlined } from '@ant-design/icons'
import { BACKEND_URL, EmployeeContext, UserContext } from '../../../../App'
import moment from 'moment'
import Papa from 'papaparse'
import axios from 'axios'

const { TabPane } = Tabs
const { Option } = Select
const { Dragger } = Upload
interface Employee {
  name: string
  job: string
  entry_date: string
}

function Create(props: any) {
  const [confirmLoading, setConfirmLoading] = useState(false)
  const [form] = Form.useForm()
  const [submitDisabled, setSubmitDisabled] = useState(true)
  const [activeTabKey, setActiveTabKey] = useState('1')
  const [fileList, setFileList] = useState<any>([])
  const [userList, setUserList] = useState<Employee[]>([])

  const { employeeInfos, dispatch } = useContext(EmployeeContext)

  const { user } = useContext(UserContext)

  const handleOk = () => {
    if (user.userInfo.role === 'Admin') {
      setConfirmLoading(true)

      let employees: Employee[] = []

      if (activeTabKey === '1') {
        const userInfo = form.getFieldsValue()
        userInfo.entry_date = userInfo.entry_date.format('YYYY-MM-DD')
        employees.push(userInfo)
      } else {
        employees = [...userList]
      }
      axios({
        method: 'post',
        url: `http://${BACKEND_URL}/employees`,
        data: { data: employees },
      })
        .then((res) => {
          const data = res.data
          if (data.total > 1) {
            message.success(`${data.total} Employees Created`)
          } else {
            message.success(`${data.total} Employee Created`)
          }

          const page = employeeInfos.pagination.current
          const pageSize = employeeInfos.pagination.pageSize
          return axios({
            method: 'get',
            url: `http://${BACKEND_URL}/employees/filter?page=${page}&pageSize=${pageSize}`,
            params: { ...employeeInfos.filters },
          })
        })
        .then((response) => {
          dispatch(getEmployee(response.data))
        })
        .catch((err) => {
          console.log(err)
          message.error('Create Employee Failed')
        })
        .finally(() => {
          if (activeTabKey === '1') {
            form.resetFields()
          } else {
            setFileList([])
          }
          setConfirmLoading(false)
          setSubmitDisabled(true)
        })
    } else {
      message.error('Create Failed, Administrator Right is Required')
    }
  }

  const handleCancel = () => {
    console.log('Clicked cancel button')
    props.setVisible(false)
    setSubmitDisabled(true)
    if (activeTabKey === '1') {
      form.resetFields()
    } else {
      setFileList([])
    }
  }

  const formOnChange = () => {
    if (form.isFieldsTouched()) {
      const userInfo = form.getFieldsValue()
      //check the form value to allow the modal submit button
      if (userInfo.name.trim() !== '' && userInfo.job && userInfo.entry_date) {
        setSubmitDisabled(false)
      } else {
        setSubmitDisabled(true)
      }
    }
  }

  const onTabChange = (activeKey: string) => {
    setActiveTabKey(activeKey)
    setSubmitDisabled(true)
    if (activeKey === '2') {
      form.resetFields()
    } else {
      setFileList([])
    }
  }

  const disabledDate = (current: any) => {
    return current && current > moment().endOf('days')
  }

  const single_add = (
    <Form
      initialValues={{ name: '' }}
      form={form}
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
      labelAlign="left"
    >
      <Form.Item name="name" label="Name">
        <Input placeholder="String Only" onChange={formOnChange} />
      </Form.Item>

      <Form.Item name="job" label="Job Description">
        <Select placeholder="Select" onChange={formOnChange} allowClear>
          <Option value="Front-End Dev">Front-End Dev</Option>
          <Option value="Core Server">Core Server</Option>
          <Option value="Product Manager"> Product Manager</Option>
          <Option value="Project Manager"> Project Manager</Option>
          <Option value="QA">QA</Option>
          <Option value="Dev Ops">Dev Ops</Option>
        </Select>
      </Form.Item>
      <Form.Item name="entry_date" label="Entry Date">
        <DatePicker
          placeholder="Date"
          style={{ width: '100%' }}
          onChange={formOnChange}
          disabledDate={disabledDate}
        />
      </Form.Item>
    </Form>
  )

  const dragProps = {
    accept: '.csv',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',

    beforeUpload: (file: any) => {
      console.log(file)

      Papa.parse(file, {
        header: true,
        complete: function (result) {
          const data = result.data
          //fitler the data, delete the rows with missing fields
          const dataAfterFilter: any[] = data.filter((employee: any) => {
            const properties = ['name', 'job', 'entry_date']
            for (const property of properties) {
              if (
                employee[property] === '' ||
                employee[property] === undefined
              ) {
                return false
              }
            }
            return true
          })
          console.log(dataAfterFilter)
          setUserList(dataAfterFilter)
        },
      })
      setFileList([file])
      setSubmitDisabled(false)
      return false
    },

    onRemove: () => {
      setFileList([])
      setUserList([])
      setSubmitDisabled(true)
    },
    fileList,
  }

  const batch_add = (
    <Dragger {...dragProps} style={{ height: '100%' }}>
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">
        Click or drag file to this area to upload
      </p>
    </Dragger>
  )

  return (
    <Modal
      visible={props.visible}
      title="Create User"
      onOk={handleOk}
      confirmLoading={confirmLoading}
      onCancel={handleCancel}
      okText="Submit"
      width={650}
      okButtonProps={{ disabled: submitDisabled }}
    >
      <Tabs
        tabPosition="left"
        style={{ height: '300px' }}
        onChange={onTabChange}
      >
        <TabPane tab="Single Create" key="1">
          {single_add}
        </TabPane>
        <TabPane tab="Mass Create" key="2">
          {batch_add}
        </TabPane>
      </Tabs>
    </Modal>
  )
}

export default Create
