import React, { useContext } from 'react'
import {
  Form,
  Input,
  Button,
  DatePicker,
  Row,
  Col,
  Select,
  message,
} from 'antd'
import styles from './Filter.scss'
import { BACKEND_URL, EmployeeContext } from '../../../../App'
import {
  removeFilters,
  applyFitlers,
  getEmployee,
} from '../../../HookStateManagement/action'
import axios from 'axios'

const { Option } = Select
const { RangePicker } = DatePicker

function Filter() {
  const [form] = Form.useForm()
  const { employeeInfos, dispatch } = useContext(EmployeeContext)

  const onFinish = (values: any) => {
    let fieldsCheckPassed = false
    const filterConditions: any = {}

    console.log('Success:', values)
    const fieldValues = {
      ...values,
    }

    if (fieldValues.name !== undefined && fieldValues.name.trim() !== '') {
      fieldValues.name = fieldValues.name.trim()
      fieldsCheckPassed = true
      filterConditions.name = fieldValues.name
    }

    if (fieldValues.job !== undefined && fieldValues.job !== '') {
      fieldsCheckPassed = true
      filterConditions.job = fieldValues.job
    }

    if (fieldValues.entry_date && fieldValues.entry_date.length === 2) {
      const start = fieldValues.entry_date[0]
      const end = fieldValues.entry_date[1]

      if (start.add(2, 'years').isBefore(end)) {
        fieldsCheckPassed = false
        message.error("Entry Date Range shouldn't longer than 2 years")
      } else {
        fieldsCheckPassed = true
      }
      start.subtract(2, 'years') //cancel the effect in above if statement

      if (fieldsCheckPassed) {
        filterConditions.start_date = start.format('YYYY-MM-DD')
        filterConditions.end_date = end.format('YYYY-MM-DD')
      }
    }

    if (fieldsCheckPassed) {
      dispatch(applyFitlers({ ...filterConditions }))

      const page = employeeInfos.pagination.current
      const pageSize = employeeInfos.pagination.pageSize
      console.log(employeeInfos.filters)

      axios({
        method: 'get',
        url: `http://${BACKEND_URL}/employees/filter?page=${page}&pageSize=${pageSize}`,
        params: { ...filterConditions },
      })
        .then((response) => {
          console.log(response)
          dispatch(getEmployee(response.data))
        })
        .catch((error) => {
          console.log(error)
          message.error('Failed to Perform Filtering')
        })
    } else {
      if (!fieldValues.entry_date || fieldValues.entry_date !== 2) {
        message.error('Please input at least 1 filter condition')
      }
    }
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }

  const onResetClicked = () => {
    form.resetFields()
    dispatch(removeFilters())

    const page = employeeInfos.pagination.current
    const pageSize = employeeInfos.pagination.pageSize
    console.log(employeeInfos.filters)
    axios({
      method: 'get',
      url: `http://${BACKEND_URL}/employees/filter?page=${page}&pageSize=${pageSize}`,
      params: {},
    })
      .then((response) => {
        // console.log(response);
        dispatch(getEmployee(response.data))
      })
      .catch((error) => {
        console.log(error)
        message.error('Failed to Perform Filtering')
      })
  }

  return (
    <div className={styles.filter}>
      <div className={styles.tableTitle}>Table Page</div>
      <Form
        name="basic"
        initialValues={{
          name: '',
          job: '',
          entry_date: [],
        }}
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        className={styles.form}
      >
        <Row justify="space-between">
          <Col span={11}>
            <Form.Item
              labelCol={{ span: 6 }}
              labelAlign="left"
              label="Name"
              name="name"
              rules={[]}
            >
              <Input placeholder="String only" />
            </Form.Item>
          </Col>
          <Col span={11}>
            <Form.Item
              labelAlign="left"
              labelCol={{ span: 6 }}
              label="Job Description"
              name="job"
              rules={[]}
            >
              <Select placeholder="Select" allowClear={true}>
                <Option value="Front-End Dev">Front-End Dev</Option>
                <Option value="Core Server">Core Server</Option>
                <Option value="Product Manager"> Product Manager</Option>
                <Option value="Project Manager"> Project Manager</Option>
                <Option value="QA">QA</Option>
                <Option value="Dev Ops">Dev Ops</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row justify="space-between">
          <Col span={11}>
            <Form.Item
              labelCol={{ span: 6 }}
              labelAlign="left"
              label="Entry Date"
              name="entry_date"
              rules={[]}
            >
              <RangePicker
                style={{ width: '100%' }}
                placeholder={['From', 'To']}
              />
            </Form.Item>
          </Col>
          <Col span={11} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button style={{ marginLeft: '8px' }} onClick={onResetClicked}>
              Reset
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default Filter
