import React, {
  createContext,
  useContext,
  useState,
  useRef,
  useEffect,
} from 'react'
import { Table, Button, Popconfirm, Input, Form, message } from 'antd'
import Create from '../Modal/Create'
import moment from 'moment'
import {
  getEmployee,
  updateEmployeeSuccess,
  updateEmployeeFailed,
} from '../../../HookStateManagement/action'
import { BACKEND_URL, EmployeeContext, UserContext } from '../../../../App'
import axios from 'axios'

const EditableContext = createContext<any>(null)

function EditableRow({ ...props }: any) {
  // console.log(index);
  const [form] = Form.useForm()
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  )
}

function EditableCell({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}: {
  title: any
  editable: any
  children: any
  dataIndex: any
  record: any
  handleSave: any
}) {
  const [editing, setEditing] = useState(false)
  const inputRef: any = useRef(null)
  const form: any = useContext(EditableContext)

  useEffect(() => {
    if (editing) {
      inputRef.current.focus()
    }
  }, [editing])

  const toggleEdit = () => {
    setEditing(!editing)
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    })
  }

  const save = async () => {
    try {
      const values = await form.validateFields()
      toggleEdit()
      handleSave({ ...record, ...values })
    } catch (errInfo) {
      console.log('Save Failed: ', errInfo)
    }
  }

  let childNode = children

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{ margin: 0 }}
        name={dataIndex}
        rules={[{ required: true, message: `${title} is required` }]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{ paddingRight: 24 }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    )
  }

  return <td {...restProps}>{childNode}</td>
}

function Display() {
  const [visible, setVisible] = useState(false)
  const userContext: any = useContext(UserContext)
  const user = userContext.user
  const { employeeInfos, dispatch } = useContext(EmployeeContext)

  useEffect(() => {
    const page = employeeInfos.pagination.current
    const pageSize = employeeInfos.pagination.pageSize

    axios({
      method: 'get',
      url: `http://${BACKEND_URL}/employees?page=${page}&pageSize=${pageSize}`,
    })
      .then((response) => {
        console.log(response.data)
        dispatch(getEmployee(response.data))
      })
      .catch((error) => {
        console.log(error)
        message.error('Featch Data Error')
      })
  }, [])

  const showModal = () => {
    setVisible(true)
  }

  const handleDelete2 = async (record: { _id: string }) => {
    const page = employeeInfos.pagination.current
    const pageSize = employeeInfos.pagination.pageSize

    try {
      axios({
        method: 'delete',
        url: `http://${BACKEND_URL}/employees`,
        data: {
          _id: record._id,
        },
      })
        .then(() => {
          return axios({
            method: 'get',
            url: `http://${BACKEND_URL}/employees/filter?page=${page}&pageSize=${pageSize}`,
            params: { ...employeeInfos.filters },
          })
        })
        .then((response) => {
          dispatch(getEmployee(response.data))
        })
    } catch (error) {
      message.error('Delete Employee Error')
    }
  }

  const handleSave2 = (row: { _id: string; name: string }) => {
    axios({
      method: 'put',
      url: `http://${BACKEND_URL}/employees`,
      data: {
        _id: row._id,
        name: row.name,
      },
    })
      .then((response) => {
        dispatch(updateEmployeeSuccess(response.data))
      })
      .catch((err) => {
        console.log(err)
        message.error('Update Employee Data Error')
        dispatch(updateEmployeeFailed())
      })
  }

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  }
  const now = moment()

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '28%',
      editable: user.userInfo.role === 'Admin',
      onCell: (record: any) => ({
        record,
        editable: user.userInfo.role === 'Admin',
        dataIndex: 'name',
        title: 'Name',
        handleSave: handleSave2,
      }),
    },
    {
      title: 'Job Description',
      dataIndex: 'job',
      key: 'job',
      width: '28%',
    },
    {
      title: 'Duration',
      key: 'entry_date',
      dataIndex: 'entry_date',
      width: '28%',
      render: (entry_date: string) => {
        const diffs = now.diff(entry_date, 'months')
        if (diffs === 0) {
          return 'Less than 1 month'
        } else if (diffs < 12) {
          if (diffs === 1) {
            return `${diffs} month`
          } else {
            return `${diffs} months`
          }
        } else {
          const years = Math.floor(diffs / 12)
          const months = diffs % 12
          if (months !== 0) {
            if (years === 1 && months === 1) {
              return `${years} year ${months} month`
            } else if (years === 1) {
              return `${years} year ${months} months`
            } else if (months === 1) {
              return `${years} years ${months} month`
            } else {
              return `${years} years ${months} months`
            }
          } else {
            if (years === 1) {
              return `${years} years`
            } else {
              return `${years} year`
            }
          }
        }
      },
    },
    {
      title: 'Action',
      key: 'action',
      render: (text: any, record: any) =>
        data.length >= 1 ? (
          <Popconfirm
            title={
              user.userInfo.role === 'Admin'
                ? 'Are you sure to delete?'
                : 'Only admin user can delete!'
            }
            onConfirm={() => handleDelete2(record)}
          >
            <a>Delete</a>
          </Popconfirm>
        ) : null,
    },
  ]

  const data = employeeInfos.data

  const handleTableChange = (new_pagination: any) => {
    const page = new_pagination.current
    const pageSize = new_pagination.pageSize
    axios({
      method: 'get',
      url: `http://${BACKEND_URL}/employees/filter?page=${page}&pageSize=${pageSize}`,
      params: { ...employeeInfos.filters },
    }).then((response) => {
      dispatch(getEmployee(response.data))
    })
  }

  return (
    <div>
      <Button onClick={showModal} type="primary" style={{ marginBottom: 16 }}>
        Create
      </Button>
      <Create visible={visible} setVisible={setVisible} />
      <Table
        loading={employeeInfos.loading}
        components={components}
        columns={columns}
        dataSource={data}
        pagination={employeeInfos.pagination}
        onChange={handleTableChange}
        rowKey="_id"
        scroll={{ y: 450 }}
      />
    </div>
  )
}

export default Display
