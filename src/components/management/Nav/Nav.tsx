import React, { useContext } from 'react'
import { Button, Avatar, Popover, Space, Image, Layout } from 'antd'
import { MenuOutlined, LoginOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom'
import styles from './Nav.scss'
import { UserContext } from '../../../App'
import { userLogOut } from '../../HookStateManagement/userAction'

const { Header } = Layout

function Nav() {
  const { user, userDispatch } = useContext(UserContext)
  const history = useHistory()
  const Logout = () => {
    userDispatch(userLogOut())
    history.push('/login')
  }

  const Login = () => {
    history.push('/login')
  }
  const content = (
    <Space direction="vertical">
      <Space direction="horizontal">
        {user.userInfo.image ? (
          <Avatar src={<Image src={user.userInfo.image} />} />
        ) : (
          <Avatar
            src={
              <Image src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            }
          />
        )}
        <Space direction="vertical">
          <div className={styles.userName}>{user.userInfo.name}</div>
          <div className={styles.userEmail}>{user.userInfo.email}</div>
        </Space>
      </Space>
      <Button className={styles.logoutButton} onClick={Logout}>
        <LoginOutlined />
        Log Out
      </Button>
    </Space>
  )

  return (
    <Header className={styles.navbar}>
      <div className={styles.left}>
        <MenuOutlined />
        <div className={styles.title}>Information Management System</div>
      </div>
      <div>
        {user.isAuthenticated ? (
          <Popover content={content} trigger="click" style={{ padding: '0px' }}>
            <Avatar className={styles.avatar}>
              {user.userInfo.name[0].toUpperCase()}
              {/* U */}
            </Avatar>
          </Popover>
        ) : (
          <Button type="primary" onClick={Login}>
            Log In
          </Button>
        )}
      </div>
    </Header>
  )
}

export default Nav
