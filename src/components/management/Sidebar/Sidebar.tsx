import React from 'react'
import styles from './Sidebar.scss'
import { Menu, Layout } from 'antd'
import { PushpinOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom'

const { Sider } = Layout
const rootSubmenuKeys = ['sub1']
const { SubMenu } = Menu

function Sidebar() {
  const [openKeys, setOpenKeys] = React.useState(['sub1'])
  const history = useHistory()

  const onOpenChange = (keys: any) => {
    const latestOpenKey = keys.find((key: any) => openKeys.indexOf(key) === -1)
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys)
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : [])
    }
  }

  const itemOnClick = (e: any) => {
    console.log(e.key)
    if (e.key === '1') {
      history.push('/management/table')
    } else if (e.key === '2') {
      history.push('/management/chart')
    }
  }

  return (
    <Sider className={styles.sidebar} width={'18vw'}>
      <Menu
        mode="inline"
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        className={styles.menu}
      >
        <SubMenu
          key="sub1"
          icon={<PushpinOutlined />}
          title="System Management"
        >
          <Menu.Item key="1" onClick={itemOnClick}>
            Table Page
          </Menu.Item>
          <Menu.Item key="2" onClick={itemOnClick}>
            Chart Page
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  )
}

export default Sidebar
