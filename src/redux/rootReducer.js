import { combineReducers } from 'redux'
import employeeReducer from './employees/employeeReducers'
import userReducer from './user/userReducer'

const rootReducer = combineReducers({
  employees: employeeReducer,
  user: userReducer,
})

export default rootReducer
