import {
  USER_LOGIN_REQUESTED,
  USER_LOGIN_FAILED,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
} from './userTypes'

const initialState = {
  loading: false,
  userInfo: {},
  isAuthenticated: false,
  error: '',
}

// @ts-ignore
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUESTED:
      return {
        ...state,
        loading: true,
        error: '',
      }

    case USER_LOGIN_SUCCESS:
      return {
        loading: false,
        userInfo: action.payload,
        isAuthenticated: true,
        error: '',
      }

    case USER_LOGIN_FAILED:
      return {
        loading: false,
        userInfo: {},
        isAuthenticated: false,
        error: action.payload,
      }

    case USER_LOGOUT:
      return {
        loading: false,
        userInfo: {},
        isAuthenticated: false,
        error: '',
      }
    default:
      return state
  }
}

export default reducer
