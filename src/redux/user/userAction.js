import {
  USER_LOGIN_REQUESTED,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  USER_LOGOUT,
} from './userTypes'

//simulate user login
// @ts-ignore
function timeout(username, password) {
  // @ts-ignore
  return new Promise((resolve, reject) => {
    if (username === 'qiang.yi@shopee.com' && password === '123456789') {
      setTimeout(resolve, 100, {
        name: 'yiqiang',
        email: 'qiang.yi@shopee.com',
        role: 'Admin',
        image: '',
      })
    } else if (username === 'qiang.yi@shopee.com') {
      setTimeout(reject, 100, 'Wrong Password')
    } else {
      setTimeout(reject, 100, 'You are not registered')
    }
  })
}

// @ts-ignore
export const user_login = (username, password) => {
  // @ts-ignore
  return (dispatch) => {
    dispatch(user_login_requested())
    timeout(username, password)
      .then((response) => {
        dispatch(user_login_success(response))
      })
      .catch((error) => {
        dispatch(user_login_failed(error))
      })
  }
}

export const user_login_requested = () => {
  return {
    type: USER_LOGIN_REQUESTED,
  }
}

// @ts-ignore
export const user_login_success = (userInfo) => {
  return {
    type: USER_LOGIN_SUCCESS,
    payload: userInfo,
  }
}

// @ts-ignore
export const user_login_failed = (error) => {
  return {
    type: USER_LOGIN_FAILED,
    payload: error,
  }
}

export const user_logout = () => {
  return {
    type: USER_LOGOUT,
  }
}
