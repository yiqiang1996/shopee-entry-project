import { ADD_EMPLOYEE, DELETE_EMPLOYEE, EDIT_EMPLOYEE } from './employeeTypes'

const initialState = {
  count: 4,
  data: [
    { key: 0, name: 'yiqiang', job: 'Frontend_Dev', entry_date: '2021-07-12' },
    { key: 1, name: 'zhangsan', job: 'Dev Ops', entry_date: '2021-07-13' },
    {
      key: 2,
      name: 'xiaoming',
      job: 'Product Manager',
      entry_date: '2021-07-14',
    },
    {
      key: 3,
      name: 'xiaomingmama',
      job: 'Frontend_Dev',
      entry_date: '2021-07-15',
    },
    // { key: 4, name: 'yiqiang', job: 'Frontend_Dev', entry_date: '2021-07-16' },
    // { key: 5, name: 'yiqiang', job: 'Frontend_Dev', entry_date: '2021-07-17' },
    // { key: 6, name: 'yiqiang', job: 'Frontend_Dev', entry_date: '2021-07-18' },
    // { key: 7, name: 'yiqiang', job: 'Frontend_Dev', entry_date: '2021-07-19' },
  ],
}

// @ts-ignore
const employeeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_EMPLOYEE:
      return {
        ...state,
        count: state.count + 1,
        // @ts-ignore
        data: [...state.data, action.payload],
      }
    case DELETE_EMPLOYEE:
      return {
        ...state,
        // @ts-ignore
        count: state.count - 1,
        data: state.data.filter((item) => item.key !== action.payload.key),
      }
    case EDIT_EMPLOYEE:
      const newData = [...state.data]
      const index = newData.findIndex((item) => action.payload.key === item.key)
      const item = newData[index]
      newData.splice(index, 1, { ...item, ...action.payload })
      return {
        ...state,
        data: newData,
      }
    default:
      return state
  }
}

export default employeeReducer
