import { ADD_EMPLOYEE, DELETE_EMPLOYEE, EDIT_EMPLOYEE } from './employeeTypes'
import { v4 as uuidv4 } from 'uuid'

// @ts-ignore
export const add_employee = (employee) => {
  employee = { ...employee, key: uuidv4() }
  return {
    type: ADD_EMPLOYEE,
    payload: employee,
  }
}

// @ts-ignore
export const delete_employee = (employee) => {
  return {
    type: DELETE_EMPLOYEE,
    payload: employee,
  }
}

// @ts-ignore
export const update_employee = (employee) => {
  return {
    type: EDIT_EMPLOYEE,
    payload: employee,
  }
}

// @ts-ignore
// export const get_employee=(filters)=>{
//     return{
//         type:get_employee,
//         payload:filters
//     }
// }
