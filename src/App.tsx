import React, { useReducer, useEffect, Suspense, lazy } from 'react'
// import LoginForm from './components/Login/LoginForm'
// import Management from './components/management/management'
import 'antd/dist/antd.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'
import axios from 'axios'

import { reducer } from './components/HookStateManagement/reducer'
import { userReducer } from './components/HookStateManagement/userReducer'

const LoginForm = lazy(() => import('./components/Login/LoginForm'))
const Management = lazy(() => import('./components/management/management'))

const employeeInitialState = {
  loading: false,
  data: [],
  error: '',
  pagination: {
    current: 1,
    pageSize: 5,
    defaultPageSize: 5,
    pageSizeOptions: [5, 10, 20, 50],
    showQuickJumper: true,
    showSizeChanger: true,
    total: 0,
  },
  filters: {},
}

export const BACKEND_URL = '54.255.155.98'

export const EmployeeContext = React.createContext<any>(null)

const userInitialState = {
  loading: false,
  userInfo: {},
  isAuthenticated: false,
  error: '',
}
export const UserContext = React.createContext<any>(null)

export const App = () => {
  const [employeeInfos, dispatch] = useReducer(reducer, employeeInitialState)
  const [user, userDispatch] = useReducer(userReducer, userInitialState)

  //auto create a new user in mongodb for login purpose
  useEffect(() => {
    axios({
      method: 'post',
      url: `http://${BACKEND_URL}/users`,
      data: {
        username: 'admin',
        password: 'admin123',
        email: 'admin_user@shopee.com',
        role: 'Admin',
      },
    })
      .then(() => {
        console.log('Admin User Created')
      })
      .catch((e) => {
        console.log(e)
      })
  }, [])

  return (
    <UserContext.Provider value={{ user, userDispatch }}>
      <EmployeeContext.Provider value={{ employeeInfos, dispatch }}>
        <Router>
          <div className="App">
            <Suspense fallback={<div>Loading ....</div>}>
              <Switch>
                <Route exact path="/">
                  <Redirect to="/login" />
                </Route>
                <Route exact path="/login">
                  <LoginForm />
                </Route>
                <Route path="/management">
                  <Management />
                </Route>
                <Route path="*">
                  <Redirect to="/login" />
                </Route>
              </Switch>
            </Suspense>
          </div>
        </Router>
      </EmployeeContext.Provider>
    </UserContext.Provider>
  )
}
