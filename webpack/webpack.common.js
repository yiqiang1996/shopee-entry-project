const MiniCssExtractPlugin =require ('mini-css-extract-plugin')

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: path.resolve(__dirname, '..', './src/index.tsx'),
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ]
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          // {
          //   loader:'postcss-loader',
          //   options: {
          //     postcssOptions:{
          //       plugins:[
          //         [
          //           'postcss-preset-env', 
          //         ]
          //       ]
          //     }
          //   }
          // }
        ],
      },
      {
        test:/\.scss$/,
        use:[
          MiniCssExtractPlugin.loader,
          {
            loader:'css-loader',
            options:{
              modules:true,
              importLoaders:1
            }
          },
          {
            loader:'postcss-loader',
            options: {
              postcssOptions:{
                plugins:[
                  [
                    "postcss-preset-env",
                  ]
                ]
              }
            }
          },
          {
            loader:"sass-loader",
            options: {
              sourceMap:true
            }
          }
        ]
      },
      {
        test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
        type: 'asset/inline',
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, '..', './build'),
    filename: 'js/[name].[contenthash:10].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '..', './src/index.html'),
    }),
    new MiniCssExtractPlugin({
      filename:"css/[name].css",
      chunkFilename:"css/[name].chunk.css"
    })
  ],
//   stats: 'errors-only',
}