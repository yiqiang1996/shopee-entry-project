const webpack = require ('webpack');
const {CleanWebpackPlugin} =require('clean-webpack-plugin');
const CssMinimizerPlugin=require('css-minimizer-webpack-plugin');
const BundleAnalyzerPlugin=require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const UglifyjsPlugin=require('uglifyjs-webpack-plugin');


module.exports={
    mode:'production',
    devtool: false,
    plugins:[
        new webpack.DefinePlugin({
            'process.env.name':JSON.stringify("YIQIANG")
        }),
        
        new webpack.IgnorePlugin({
            resourceRegExp: /^\.\/locale$/,
            contextRegExp: /moment$/,
        }),
        new CleanWebpackPlugin(),
        new BundleAnalyzerPlugin({analyzerPort:8000})
    ],
    optimization:{
        minimizer:[
            `...`,
            new UglifyjsPlugin({
                cache:true,
                parallel:true,
                sourceMap:true
            }),
            new CssMinimizerPlugin(),
        ],

        // splitChunks:{
        //     chunks:'all'
        // }
    }
}