#Shopee Entry Project
===================================

## Environments required:
node v14.15.4

## Steps to Run and Build the Project

1. npm install  

2. npm start    

3. npm run build 


## **Technical Requirement Checklists**

### Git
- [x] Create a git repo on gitlab
- [x] Commit frequently with meaningful commit messages
- [x] Meaningful branch
- [ ] Setup Gitlab CI to check your merge request

### Dev and Build
- [x] Use webpack as your build tool 
- [x] Cannot use any boilerplate generator like "create-react-app"
- [x] Should have seperate configuration for local development and production build
    - Production mode should satisfy these requirement
        - Javascript
            - [x] Uglify
            - [x] Minify
            - [x] Tree shaking
            - [ ] Code split
            - [ ] Bundle common library
        - CSS
            - [x] Minify
            - [x] Optimize
            - [x] Auto Prefixer
- [ ] (Bounus) Use Docker to mimic the process of deploy in your own computer

### React/React Route/State Management
- [x] Use React 16.8.0+
- [x] Use React Hooks
- [x] Use 'React Router' for routing
- [x] Redux is optional. (tried react-redux)
- [x] Handle the different states of your results table for loading data (loading, success, error) with sensible UI

### Typescript
- [x] Use 'TypeScript' as a static  type checker to check your data structure and functions
- [ ] Document and type API schema with 'Typescript'
- [x] Your tsconfig should turn on 'strict' mode.

### Styles and Layout 
- [x] Use Ant Design in your project
- [x] Use SASS as preprocessor
- [x] Use Your CSS Module to style your react component
- [x] Add autoprefixer to cover browser discrepency on CSS implementation

### Linting
- [x] Setup ESLint as a 'pre-commit' hook
- [x] Use 'prettier' to format your code automatically
- [ ] Use 'stylelint' as a 'pre-commit' hook to your CSS code as well

### Unit Test
- [ ] (Bonus) Use jest or other front-end unit test library
- [ ] (Bonus) Test Coverage Report


## **Knowledge Checklist**

- [x] React
    - [x] React-Router
    - [x] React-Hooks
    - [x] Redux (optional)
- [x] CSS
    - [x] Antd
    - [x] SASS
    - [x] CSS Module
- [x] Typescript
- [x] Webpack
    - [x] Dev/Prod mode
    - [x] Common Plugins
- [x] Linting
- [ ] Unit Test


## **Product Requirement Checklist**

### Login Page:

- [x] Unathenticated user will be redirect to this page 
- [x] Implement the login page on your own, no design provided

### Main Page:
- [x] Main Page should include **filter** and **table**
- **Filter**:
    - [x] Name is a text input element
    - [x] Job Description is dropdown selector element
    - [x] Entry Date is date range picker, the results will return employees joined the organization during the period of the range selected
    - [x] Clicking the form on Submit button should submit the form to perform a search
    - [x] Clicking the Reset button should empty all fields and display the search results table in its default state (the default state without any filters applied)
- [x] **Form Validation**: At least 1 filter field needs to be filled to perform a search; Entry Date range shouldn't be longer than 2 years. Any form validation errors should trigger a prompt to user as shown int the design.
- [x] **Table**: Columns: Name (editable), Job Description, Duration (how long has it been since this employee joined), and Action (delete)
- [x] **Creation of new Entry**:  Clicking on the Create button will open a modal with 2 ways to create: single manual creation or mass create with CSV file.

### (Bonus) Chart Page:
- [x] Implemented a simple chart using antd/charts

### Access Control:
- [x] Users with access permission can access the main and chart page. Users without access permission cannot acccess these pages and will be redirected back to the login page with a no-permission error UI.
- (Bonus) For users with access permission, they can either be Admin or ordinary user.
    - [x] Only Admin are able to create, edit and delete employee data
    - [x] Users only have permission to view are not allowed to write
- [ ] (Bonus) Use Google Auth as authentication


## **Project Structure**
```
Entry Task
|   build
|   node_modules
│   readme.md
│   .babelrc
|   .eslintrc
|   .gitignore
|   .prettierrc.js
|   package-lock.json
|   package.json
|   tsconfig.json    
│
└───src
│   │   index.tsx
│   │   index.html
|   |   index.css
|   |   declarations.d.ts
|   |   App.tsx
|   |   _font.scss
│   │
│   └───redux
│   |   │   index.js
│   |   │   rootReducer.js
│   |   │   store.js
|   |   |
│   |   └───employees
|   |   |   |   employeeActions.js
|   |   |   |   employeeReducers.js
|   |   |   |   employeeTypes.js
|   |   |
|   |   └───user
|   |   |   |   userAction.js
|   |   |   |   userReducer.js
|   |   |   |   userTypes.js
|   |
|   └───components
|       |
|       └───HookStateManagement
|       |   |   action.ts
|       |   |   reducer.ts
|       |   |   userAction.ts
|       |   |   userReducer.ts
|       |
|       └───Login
|       |   |   LoginForm.scss
|       |   |   LoginForm.tsx
|       |
|       └───management
|           |   management.tsx
|           |   
|           └───Chart
|           |   |   Chart.tsx
|           |   |   chart.scss
|           |   
|           └───Nav
|           |   |   Nac.scss
|           |   |   Nac.tsx
|           |   
|           └───Sidebar
|           |   |   Sidebar.scss
|           |   |   Sidebar.tsx
|           |   
|           └───Table
|               |   Table.scss
|               |   Table.tsx
|               |
|               └───Filter
|               |   |   Filter.tsx
|               |   |   Filter.scss
|               |
|               └───Display
|               |   |   Display.tsx
|               |  
|               └───Modal
|               |   |   Create.tsx
└───webpack
    │   webpack.common.js
    │   webpack.config.js
    |   webpack.dev.js
    |   webpack.prod.js
```